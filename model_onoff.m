

%% Rule based control
%  On-off hystheresis controller

% nacitaj stavovy model budovy
load ('ss_model.mat')

% nacitaj historicke profily poruch
load ('wpred_1979_may_Ts300')

% zakladna perioda vzorkovania dat
base_Ts = y(2,1);
% profily poruch
Disturb = [y(:,4)'; y(:,end-1)'; y(:,end)']; % T_ext, Q_occ, Q_sol

% vykreslenie casovych priebehov poruch
figure
plot(Disturb')
title('Disturbances')
legend('T external','Q occupancy','Q solar')

% POPIS vyznamu stavov vstupov a poruch
% states:
%  x1: T_floor
%  x2: T_facade_internal
%  x3: T_facade_external
%  x4: T_internal
% input:
%  u1: Q_heating_cooling
% disturbances:
%  d1: T_external
%  d2: Q_occupancy
%  d3: Q_solar


% MIMO model continuous
model1 = ss(A,B,C,D);

% % SISO model 1. vstup na 1. vystup
% Ak = A;
% Bk = B(:,2);
% Ck = C(4,:);
% Ek = B(:,[1 3 4]);
% Dk = 0;

% % SISO model
% % [num, den] = ss2tf(Ak,Bk,Ck,Dk);
% % model2 = tf(num,den);
% % [An,Bn,Cn,Dn] = tf22ss(num, den);
% % spojity stavovy model SISO
% model_spoj = ss(Ak,Bk,Ck,Dk);


% diskretizacia
Ts = 15;
sd = c2d(ss(A, B, C, D), Ts);
% Spomalenie dynamiky modelu!!!???
Ad = sd.A;
Bd = sd.B(:, 2);
Cd = sd.C(4,:);
Ed = sd.B(:, [1 3 4]);
% Dd = sd.D;
Dd = 0;

% simulacne kroky
Nsim = 600;

% inicializacia hodnot
x0 = 21*ones(size(A,1),1);
u0 = 5000;
p0 = Disturb(:,1:Nsim);

% ON/OFF regulator
w = 22;
delta = 0.5;

% vektory 
X = x0;
Y = [];
U = [];
P = [];
W = [];
LB = [];
UB = [];

time = 1:Nsim;

% out of interval values counter
counter = 0;

for i=1:Nsim
    
    x0 = X(:,end);
    
    % system
    xkn = Ad*x0 + Bd*u0 + Ed*p0(:,i);
    yk = Cd*x0 + Dd*u0;
    
    % ON/OFF controller
    lb = w - delta;
    ub = w + delta;
    
    if yk < lb
        u0 = 5000;
    end
    
    if yk > ub
        u0 = 0;
    end
    
    if yk > ub | yk < lb
        counter = counter + 1;
    end
    
%     % alternative - logic of the central on-off thermostat
%     above = yk >= w + delta;
%     below = yk <= w - delta;
%     doheat = (heat & ~above) | (~heat & below);
%     heat = doheat;
        
    x0 = xkn;   

    X = [X, xkn];
    Y = [Y, yk];
    U = [U, u0];
    P = [P, p0(:,i)];
    W = [W, w];
    LB = [LB, lb];
    UB = [UB, ub];
end

% comfort
com_onoff = 100-(counter*100)/Nsim
% energy consumption
energy_onoff = sum(U)

% figure
% subplot(3,1,1)
% plot(time, X(:,1:end-1))
% title('States')
% legend('T floor','T internal facade','T external facade','T internal','location','bestoutside')
% xlabel('time')
% ylabel('temperature [\circC]')
figure
subplot(2,1,1)
plot(time, Y,'LineWidth',2)
hold on
plot(time,W,'r--','LineWidth',2)
plot(time,LB,'g--','LineWidth',2)
plot(time,UB,'g--','LineWidth',2)
title('Room temperature')
% legend('T room', 'reference','lower bound','upper bound','location','bestoutside')
xlabel('time')
ylabel('temperature [\circC]')

subplot(2,1,2)
stairs(time,U,'LineWidth',2)
title('Heating')
xlabel('time')
ylabel('P [W]')

figure

subplot(3,1,1)
plot(time, Y,'LineWidth',2)
hold on
plot(time,W,'r--','LineWidth',2)
plot(time,LB,'g--','LineWidth',2)
plot(time,UB,'g--','LineWidth',2)
title('Room Temperature')
legend('T room', 'reference','lower bound','upper bound','location','bestoutside')
xlabel('time')
ylabel('temperature [\circC]')

subplot(3,1,2)
stairs(time,U,'LineWidth',2)
title('Input')
legend('Q heating','location','bestoutside')
xlabel('time')
ylabel('Q')

subplot(3,1,3)
plot(time,P,'LineWidth',2)
title('Disturbances')
legend('T external','Q occupancy','Q solar','location','bestoutside')
xlabel('time')
ylabel('')