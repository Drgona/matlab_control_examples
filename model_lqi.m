%% LQR control of building linear state space model 

% Design of the LQR controller for simple 4 state system representing the single zone building 
% single input
% 3 disturbances

% states:
%  x1: T_floor
%  x2: T_facade_internal
%  x3: T_facade_external
%  x4: T_internal
% input:
%  u1: Q_heating_cooling
% disturbances:
%  d1: T_external
%  d2: Q_occupancy
%  d3: Q_solar

%% model and disturbances load 
% load building model
load ('ss_model.mat')
% load disturbances profiles
load ('wpred_1979_may_Ts300')
% sampling time
base_Ts = y(2,1);
%  disturbances profiles vector
Disturb = [y(:,4)'; y(:,end-1)'; y(:,end)']; % T_ext, Q_occ, Q_sol

% plotting disturnances 
plot(Disturb')
title('Disturbances')
legend('T external','Q occupancy','Q solar')

%% model discretization 
% MIMO model continuous
model1 = ss(A,B,C,D);
% discretization of the model
Ts = 15;
sd = c2d(ss(A, B, C, D), Ts);
% construction of the A,B,C,D, matrices division of original B matrix
%  to Bd (control) matrix and Ed (disturbance matrix)
Ad = sd.A;
Bd = sd.B(:, 2);
Cd = sd.C(4,:);
Ed = sd.B(:, [1 3 4]);
% Dd = sd.D;
Dd = 0;



%% OFFSET FREE LQR
% http://www.mathworks.com/help/control/ref/lqi.html
model = ss(Ad,Bd,Cd,Dd,Ts);

% simulation steps
Nsim = 600;
% initialization
x0 = 21*ones(size(A,1),1);
u0 = 0;
p0 = Disturb(:,1:Nsim);
% vectors 
X = x0;
Y = [];
U = [];
P = [];
E = [];
SE = [];
time = 1:Nsim;
% LQR controller design
nx = size(Ad,2);
ny = size(Cd,1);
nu = size(Bd,2);
% Q = 1e8*eye(nx+1);
% Q = 1e5*[Cd 1]'*[Cd 1];
Q = 5e3*[zeros(1,nx) 1]'*[zeros(1,nx) 1];
R = 1*eye(nu);
N = zeros(nx+1,nu);
w = [22*ones(1,Nsim/3) 22*ones(1,Nsim/3) 22*ones(1,Nsim/3)];
lb = [21.5*ones(1,Nsim/3) 21.5*ones(1,Nsim/3) 21.5*ones(1,Nsim/3)];
ub = [22.5*ones(1,Nsim/3) 22.5*ones(1,Nsim/3) 22.5*ones(1,Nsim/3)];
% sum_e = 0;
sum_e = 2200;

% offset free LQR - with integrator
[K,S,e] = lqi(model,Q,R,N);
% manual tuning
% K(4) = K(4)/10;
% K(1:end-1) = 0;

% out of interval values counter
counter = 0;
delta = 0.5;

%% closed loop simulation
for i=1:Nsim
    
    x0 = X(:,end);
  
    % system
    xkn = Ad*x0 + Bd*u0 + Ed*p0(:,i);
    yk = Cd*x0 + Dd*u0;

    err = Ts*(w(i) - yk);
    sum_e = sum_e + err; 
    %   LQR ctrl feedback
    u0 = -K*[x0; sum_e]; 
    if u0 < 0
        u0 = 0;
    end
    
    X = [X, xkn];
    Y = [Y, yk];
    U = [U, u0];
    P = [P, p0(:,i)]; 
    E = [E, err];
    SE = [SE, sum_e];
    
    lb = w - delta;
    ub = w + delta;
     if yk > ub | yk < lb
        counter = counter + 1;
     end
     
end

% comfort
com_lqr2 = 100-(counter*100)/Nsim
% energy consumption
energy_lqr2 = sum(U)


figure
subplot(3,1,1)
plot(time, Y,'LineWidth',2)
hold on
grid on
plot(time,w,'r--','LineWidth',2)
plot(time,lb,'g--','LineWidth',2)
plot(time,ub,'g--','LineWidth',2)
title('Room temperature')
legend('T room', 'reference')
xlabel('time')
ylabel('temperature [\circC]')

subplot(3,1,2)
stairs(time,U,'LineWidth',2)
title('Input')
legend('Q heating')
xlabel('time')
ylabel('Q')

subplot(3,1,3)
plot(time,P,'LineWidth',2)
title('Disturbances')
legend('T external','Q occupancy','Q solar')
xlabel('time')
ylabel('')