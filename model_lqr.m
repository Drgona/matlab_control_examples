
%% LQR control of building linear state space model 

% Design of the LQR controller for simple 4 state system representing the single zone building 
% single input
% 3 disturbances

% states:
%  x1: T_floor
%  x2: T_facade_internal
%  x3: T_facade_external
%  x4: T_internal
% input:
%  u1: Q_heating_cooling
% disturbances:
%  d1: T_external
%  d2: Q_occupancy
%  d3: Q_solar

%% model and disturbances load 
% load building model
load ('ss_model.mat')
% load disturbances profiles
load ('wpred_1979_may_Ts300')
% sampling time
base_Ts = y(2,1);
%  disturbances profiles vector
Disturb = [y(:,4)'; y(:,end-1)'; y(:,end)']; % T_ext, Q_occ, Q_sol

% plotting disturnances 
plot(Disturb')
title('Disturbances')
legend('T external','Q occupancy','Q solar')

%% model discretization 
% MIMO model continuous
model1 = ss(A,B,C,D);
% discretization of the model
Ts = 15;
sd = c2d(ss(A, B, C, D), Ts);
% construction of the A,B,C,D, matrices division of original B matrix
%  to Bd (control) matrix and Ed (disturbance matrix)
Ad = sd.A;
Bd = sd.B(:, 2);
Cd = sd.C(4,:);
Ed = sd.B(:, [1 3 4]);
% Dd = sd.D;
Dd = 0;

%% initialization of values
% simulation steps
Nsim = 600;
% initialization
% x0 = 21*ones(size(A,1),1);
x0 = 21*ones(size(A,1),1);
p0 = Disturb(:,1:Nsim);
% initial vectors for computed variables 
X = x0;
Y = [];
U = [];
P = [];
W = [];
% W = [];
time = 1:Nsim;
% LQR controller design
nx = size(Ad,2);
ny = size(Cd,1);
nu = size(Bd,2);
% Q = 100000*eye(nx);
Q = 240*1e6*Cd'*Cd;
R = 1*eye(nu);
N = zeros(nx,nu);
% w = 20;
w = [22*ones(1,Nsim/3) 22*ones(1,Nsim/3) 22*ones(1,Nsim/3)];             % desired reference 
lb = [21.5*ones(1,Nsim/3) 21.5*ones(1,Nsim/3) 21.5*ones(1,Nsim/3)];      % lower bound
ub = [22.5*ones(1,Nsim/3) 22.5*ones(1,Nsim/3) 22.5*ones(1,Nsim/3)];      % upper bound

% out of interval values counter
counter = 0;
% comfort zone width
delta = 0.5;

%% LQR - reference tracking design
% http://ctms.engin.umich.edu/CTMS/index.php?example=InvertedPendulum&section=ControlStateSpace
[K,S,e] = dlqr(Ad,Bd,Q,R,N); 

% % % %  PRECOMPENSATION  % % % % 
% Adding precompensation - reference in steady state
% necessary in order to keep the reference tracking
% [Nbar, N] = rscale(Ad,Bd,Cd,Dd,K)
         Z = [zeros([1,nx]) nu];
         Ninv = inv([Ad,Bd;Cd,Dd])*Z';
         Nx = Ninv(1:nx);
         Nu = Ninv(1+nx);
         Nbar=Nu + K*Nx;
% Nbar - roughly it is compesation for the order of Bd w.r.t order of Ad
% our Ad has the magnutes of roughly 1e0 and Bd roughly 1e-4
% Nbar is 1e4 and by multiplying with Bd it produces values with 1e0
% magnitudes

%% Closed loop simulation
for i=1:Nsim
    
%     initial state for each iteration
    x0 = X(:,end);
    
    %   LQR ctrl action computed via state feedback gain K and precompensated with
    %   Nbar 
    u0 = -Nbar*w(i)-K*x0;   

    % system state update
    xkn = Ad*x0 + Bd*u0 + Ed*p0(:,i);
    yk = Cd*x0 + Dd*u0;
    
%     simulation values
    X = [X, xkn];
    Y = [Y, yk];
    U = [U, u0];
    P = [P, p0(:,i)];
    W = [W, w];  
    
%     lower and upper bound
    lb = w - delta;
    ub = w + delta;
%     counter of comfort zone violations
     if yk > ub | yk < lb
        counter = counter + 1;
     end
     
end

% comfort
com_lqr1 = 100-(counter*100)/Nsim
% energy consumption
energy_lqr1 = sum(U)


%% plotting

figure
subplot(3,1,1)
plot(time, Y,'LineWidth',2)
hold on
plot(time,w,'r--','LineWidth',2)
plot(time,lb,'g--','LineWidth',2)
plot(time,ub,'g--','LineWidth',2)
title('Room temperature')
legend('T room', 'reference')
xlabel('time')
ylabel('temperature [\circC]')
grid on

subplot(3,1,2)
stairs(time,U,'LineWidth',2)
title('Input')
legend('Q heating')
xlabel('time')
ylabel('Q')

subplot(3,1,3)
plot(time,P,'LineWidth',2)
title('Disturbances')
legend('T external','Q occupancy','Q solar')
xlabel('time')
ylabel('')

% return
