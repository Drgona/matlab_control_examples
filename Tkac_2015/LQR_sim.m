
%% PID control of ss building model 

%% 
% TODO 14.10.2015
% 1, analyza dynamickeho spravania systemu - odozvy na skoky (step)
% 2, ss vs tf
% 3, continuous ss vs discrete ss
% 4, optoblog zaciname s Yalmipom 1 a 2
% http://www.kirp.chtf.stuba.sk/~kvasnica/blog/yalmip01/
% 5, Yamlip basics
% http://users.isy.liu.se/johanl/yalmip/pmwiki.php?n=Tutorials.Basics


%% 
% TODO 21.10.2015
% 1, Hamlab toolbox - stranka + simulacna schema
% 2, diskretizacia
% 3, open loop simulacie 
%     3,a simulink schema - diskretny model aj spojity
%     3,b mfile


%% 
% TODO 28.10.2015
% 1, open loop sim
%     1a - dynamicke zmeny vstupov - u
%     1b - popis plotov, lable...
% 2, PID regulator 
%     2a - implementacia v simulinku - PID blok
%     2b - design PID regulatora nejakou metodou, 
%   help pidtune
%   help pitTuner
%     2c, - teoria PID regulatorov
% 

% TODO 25.11.2015
% LQR teoria
% https://en.wikipedia.org/wiki/Linear-quadratic_regulator
% https://en.wikipedia.org/wiki/Optimal_control#Linear_quadratic_control

% TODO 2.12.2015
% LQR implementacia matlab
% http://www.mathworks.com/help/control/ref/lqr.html
% http://www.mathworks.com/help/control/ref/dlqr.html
% http://www.cds.caltech.edu/~murray/courses/cds110/wi06/lqr.pdf

% TODO 9.12.2015
% LQR reference tracking
% http://ctms.engin.umich.edu/CTMS/index.php?example=InvertedPendulum&section=ControlStateSpace
% LQR with integral part
% http://www.mathworks.com/help/control/ref/lqi.html
% Bitbucket?
% Word or Latex?

% TODO:
% protokol
% model:  1, opis stavy poruchy vstup vystup
%         2, open loop response, casova konstanta, Zosilnenie
% regulatory:  1, PID
%              2, on-off
%              3, LQR ref. tracking
%              4, LQR offset free
% implementacia: 1, blokove schemy 
%                2, matlab skript 
%                3, matlab simulink
% vysledky: 1, closed loop profiles
%           2, analyza vplyvu nastaveni: parametre PID, sirka zony pre
%           on-off, vahovanie pre LQR
%           3, porovnanie priebehov, kvalita riadenia IAE, ISE, spotreba energie,
%           komfort
% zaver: strucne slovne zhodnotenie vysledkov


% nacitaj stavovy model budovy
load ('ss_model.mat')
% nacitaj historicke profily poruch
load ('wpred_1979_may_Ts300')
% zakladna perioda vzorkovania dat
base_Ts = y(2,1);
% profily poruch
Disturb = [y(:,4)'; y(:,end-1)'; y(:,end)']; % T_ext, Q_occ, Q_sol

% vykreslenie casovych priebehov poruch
plot(Disturb')
title('Disturbances')
legend('T external','Q occupancy','Q solar')

% POPIS vyznamu stavov vstupov a poruch
% states:
%  x1: T_floor
%  x2: T_facade_internal
%  x3: T_facade_external
%  x4: T_internal
% input:
%  u1: Q_heating_cooling
% disturbances:
%  d1: T_external
%  d2: Q_occupancy
%  d3: Q_solar

% MIMO model continuous
model1 = ss(A,B,C,D);
% diskretizacia
Ts = 15;
sd = c2d(ss(A, B, C, D), Ts);
% Spomalenie dynamiky modelu!!!???
Ad = sd.A;
Bd = sd.B(:, 2);
Cd = sd.C(4,:);
Ed = sd.B(:, [1 3 4]);
% Dd = sd.D;
Dd = 0;
% simulacne kroky
Nsim = 1200;
% inicializacia hodnot
% x0 = 21*ones(size(A,1),1);
x0 = 19*ones(size(A,1),1);
p0 = Disturb(:,1:Nsim);
% vektory 
X = x0;
Y = [];
U = [];
P = [];
% W = [];
time = 1:Nsim;
% LQR controller design
nx = size(Ad,2);
ny = size(Cd,1);
nu = size(Bd,2);
% Q = 100000*eye(nx);
Q = 234*1e6*Cd'*Cd;
R = 1*eye(nu);
N = zeros(nx,nu);
% w = 20;
w = [21*ones(1,Nsim/3) 22*ones(1,Nsim/3) 21*ones(1,Nsim/3)];

%% LQR - reference tracking
% http://ctms.engin.umich.edu/CTMS/index.php?example=InvertedPendulum&section=ControlStateSpace
[K,S,e] = dlqr(Ad,Bd,Q,R,N); 

% % % %  PRECOMPENSATION  % % % % 
% Adding precompensation - reference in steady state
% necessary in order to keep the reference tracking
% [Nbar, N] = rscale(Ad,Bd,Cd,Dd,K)
         Z = [zeros([1,nx]) nu];
         Ninv = inv([Ad,Bd;Cd,Dd])*Z';
         Nx = Ninv(1:nx);
         Nu = Ninv(1+nx);
         Nbar=Nu + K*Nx;
% Nbar - zhruba je to kompenzacia radu Bd vzhladom na rad Ad
% vsimni si ze nase Ad je v 1e0 a Bd je 1e-4
% Nbar je 1e4 a pri nasobeni s Bd dava jednotky 

for i=1:Nsim
    
    x0 = X(:,end);
    %   LQR ctrl
    u0 = -Nbar*w(i)-K*x0;   
%     u0 = w(i)-K*x0; 
    % system
    xkn = Ad*x0 + Bd*u0 + Ed*p0(:,i);
    yk = Cd*x0 + Dd*u0;
   
    X = [X, xkn];
    Y = [Y, yk];
    U = [U, u0];
    P = [P, p0(:,i)];
%     W = [W, w];  
end

figure
% subplot(3,1,1)
% plot(time, X(:,1:end-1))
% title('States')
% legend('T floor','T internal facade','T external facade','T internal','location','bestoutside')
% xlabel('time')
% ylabel('temperature [\circC]')

subplot(3,1,1)
plot(time, Y,'LineWidth',2)
hold on
plot(time,w,'r--','LineWidth',2)
title('room temperature')
legend('T room', 'reference')
xlabel('time')
ylabel('temperature [\circC]')
grid on

subplot(3,1,2)
stairs(time,U,'LineWidth',2)
title('Input')
legend('Q heating-cooling')
xlabel('time')
ylabel('Q')

subplot(3,1,3)
plot(time,P,'LineWidth',2)
title('Disturbances')
legend('T external','Q occupancy','Q solar')
xlabel('time')
ylabel('')

% return

%% OFFSET FREE LQR
% http://www.mathworks.com/help/control/ref/lqi.html
model = ss(Ad,Bd,Cd,Dd,Ts);

% simulacne kroky
Nsim = 1200;
% inicializacia hodnot
x0 = 19*ones(size(A,1),1);
u0 = 0;
p0 = Disturb(:,1:Nsim);
% vektory 
X = x0;
Y = []; U = []; P = [];
E = []; SE = [];
time = 1:Nsim;
% LQR controller design
nx = size(Ad,2);
ny = size(Cd,1);
nu = size(Bd,2);
% Q = 1e8*eye(nx+1);
% Q = 1e5*[Cd 1]'*[Cd 1];
Q = 5e3*[zeros(1,nx) 1]'*[zeros(1,nx) 1];
R = 1*eye(nu);
N = zeros(nx+1,nu);
w = [21*ones(1,Nsim/3) 22*ones(1,Nsim/3) 21*ones(1,Nsim/3)];
% sum_e = 0;
sum_e = 2200;

% offset free LQR
[K,S,e] = lqi(model,Q,R,N);
% manual tuning
% K(4) = K(4)/10;
% K(1:end-1) = 0;

for i=1:Nsim
    
    x0 = X(:,end);
  
    % system
    xkn = Ad*x0 + Bd*u0 + Ed*p0(:,i);
    yk = Cd*x0 + Dd*u0;

    err = Ts*(w(i) - yk);
    sum_e = sum_e + err; 
    %   LQR ctrl
    u0 = -K*[x0; sum_e]; 
    if u0 < 0
        u0 = 0;
    end
    
    X = [X, xkn];
    Y = [Y, yk];
    U = [U, u0];
    P = [P, p0(:,i)]; 
    E = [E, err];
    SE = [SE, sum_e];
end

figure
subplot(3,1,1)
plot(time, Y,'LineWidth',2)
hold on
grid on
plot(time,w,'r--','LineWidth',2)
title('room temperature')
legend('T room', 'reference')
xlabel('time')
ylabel('temperature [\circC]')

subplot(3,1,2)
stairs(time,U,'LineWidth',2)
title('Input')
legend('Q heating-cooling')
xlabel('time')
ylabel('Q')

subplot(3,1,3)
plot(time,P,'LineWidth',2)
title('Disturbances')
legend('T external','Q occupancy','Q solar')
xlabel('time')
ylabel('')
