

%% Open loop simulation of ss building model


%% 
% TODO 14.10.2015
% 1, analyza dynamickeho spravania systemu - odozvy na skoky (step)
% 2, ss vs tf
% 3, continuous ss vs discrete ss
% 4, optoblog zaciname s Yalmipom 1 a 2
% http://www.kirp.chtf.stuba.sk/~kvasnica/blog/yalmip01/
% 5, Yamlip basics
% http://users.isy.liu.se/johanl/yalmip/pmwiki.php?n=Tutorials.Basics


%% 
% TODO 21.10.2015
% 1, Hamlab toolbox - stranka + simulacna schema
% 2, diskretizacia
% 3, open loop simulacie 
%     3,a simulink schema - diskretny model aj spojity
%     3,b mfile


%% 
% TODO 28.10.2015
% 1, open loop sim
%     1a - dynamicke zmeny vstupov - u
%     1b - popis plotov, lable...
% 2, PID regulator 
%     2a - implementacia v simulinku - PID blok
%     2b - design PID regulatora nejakou metodou, 
%   help pidtune
%   help pitTuner
%     2c, - teoria PID regulatorov
% 



% nacitaj stavovy model budovy
load ('ss_model.mat')

% nacitaj historicke profily poruch
load ('wpred_1979_may_Ts300')

% zakladna perioda vzorkovania dat
base_Ts = y(2, 1);
% profily poruch
Disturb = [y(:, 4)'; y(:, end-1)'; y(:, end)']; % T_ext, Q_occ, Q_sol

% vykreslenie casovych priebehov poruch
plot(Disturb')
title('Disturbances')
legend('T external','Q occupancy','Q solar')

% POPIS vyznamu stavov vstupov a poruch
% states:
%  x1: T_floor
%  x2: T_facade_internal
%  x3: T_facade_external
%  x4: T_internal
% input:
%  u1: Q_heating_cooling
% disturbances:
%  d1: T_external
%  d2: Q_occupancy
%  d3: Q_solar


% MIMO model continuous
model1 = ss(A,B,C,D);

% % SISO model 1. vstup na 1. vystup
% Ak = A;
% Bk = B(:,2);
% Ck = C(4,:);
% Ek = B(:,[1 3 4]);
% Dk = 0;

% % SISO model
% % [num, den] = ss2tf(Ak,Bk,Ck,Dk);
% % model2 = tf(num,den);
% % [An,Bn,Cn,Dn] = tf22ss(num, den);
% % spojity stavovy model SISO
% model_spoj = ss(Ak,Bk,Ck,Dk);


% diskretizacia
Ts = 15;
sd = c2d(ss(A, B, C, D), Ts);
% Spomalenie dynamiky modelu!!!???
Ad = sd.A;
Bd = sd.B(:, 2);
Cd = sd.C(4,:);
Ed = sd.B(:, [1 3 4]);
% Dd = sd.D;
Dd = 0;

% diskretny SISO model
siso1 = ss(Ad,Bd,Cd,Dd,Ts);

% simulacne kroky
Nsim = 1000;

% inicializacia hodnot
x0 = 21*ones(size(A,1),1);
u0 = 5000;
p0 = Disturb(:,1:Nsim);

% reference
w = 24;


% vektory 
X = x0;
Y = [];
U = [];
P = [];

time = 1:Nsim;

% OPEN LOOP simulation
for i=1:Nsim
    
    
    x0 = X(:,end);
    
    
    
%      u = Kp*error + Ki*int_error +Kd*d_error;
    
    
    xkn = Ad*x0 + Bd*u0 +Ed*p0(:,i);
    yk = Cd*x0 + Dd*u0;
    
   
%     x0 = xkn;

    X = [X, xkn];
    Y = [Y, yk];
    U = [U, u0];
    P = [P, p0(:,i)];
    
    
end

figure
subplot(3,1,1)
plot(time, X(:,1:end-1))
title('States')
legend('T floor','T internal facade','T external facade','T internal','location','bestoutside')
xlabel('time')
ylabel('temperature [\circC]')

subplot(3,1,2)
plot(time,U)
title('Input')
legend('Q heating-cooling','location','bestoutside')
xlabel('time')
ylabel('Q')

subplot(3,1,3)
plot(time,P)
title('Disturbances')
legend('T external','Q occupancy','Q solar','location','bestoutside')
xlabel('time')
ylabel('')


%% PID design

% create pid object
% help pid

% GUI pid tune
% help pidTuner
% SYNTAX
% pidTuner(siso1)
% TODO: tune + extract pid ctrl
% out: structure pid object
% pid1.Kp
% pid1.Ki
% pid1.Kd


% help pidtune
% SYNTAX
% 1/WC = roughly cl repsonse time
% pidtune(siso1,'PID',WC)

% pidtune(siso1,'PID',1/Ts)





