% TODO: FIX


%% Yalmip LQR implementation 
% sizes
nx = size(Ad,2);
ny = size(Cd,1);
nu = size(Bd,2);
np = size(Ed,2);
% prediction horizon
Npred = 20;

x = sdpvar(nx,Npred+1,'full');
u = sdpvar(nu,Npred,'full');
p = sdpvar(np,Npred,'full');
y = sdpvar(ny,Npred,'full');

% weight matrixes
Qy = 1e9;
% Q = 10*eye(nx);
Q = 234*1e6*Cd'*Cd;
R = eye(nu);
N = zeros(nx,nu);
% desired reference
w = 21;

% BUILDING of optimization problem
% initiation of constraints and objective
obj = 0;
constr = [];
for k = 1:Npred
%     model construction
   constr = [constr, x(:,k+1) == Ad*x(:,k) + Bd*u(:,k) ];  %  state update
%    + Ed*p(:,k)
   constr = [constr, y(:,k) == Cd*x(:,k) + Dd*u(:,k)]; % output equation
   
   obj = obj + x(:,k)'*Q*x(:,k) + u(:,k)'*R*u(:,k); %  traditional LQR objective
%     obj = obj + y(:,k)'*Qy*y(:,k) + u(:,k)'*R*u(:,k); %  LQR output tracking objective
%  obj = obj + (w-y(:,k))'*Qy*(w-y(:,k)) + u(:,k)'*R*u(:,k); %  LQR output reference tracking objective
end

% %  OPEN LOOP solution
% initiation
constr = constr + [(x(:,1) == 21*ones(4,1))];
% solve optimization problem
sol = optimize(constr,obj)
% values of variables
value(x);
value(u);

% %  CLOSED LOOP formulation
% construction of optimization model
ctrl = optimizer(constr,obj,[],x(:,1),u(:,1));

% %  SIMULATION LOOP
% simulacne kroky
Nsim = 1200;
% inicializacia hodnot
x0 = 21*ones(size(A,1),1);
u0 = 0;
p0 = Disturb(:,1:Nsim);
% vektory 
X = x0;
Y = [];
U = [];
P = [];
W = [];
time = 1:Nsim;

for i=1:Nsim
    
    x0 = X(:,end);
    % LQR controller
    u0 = -ctrl{x0};
    
    % system
    xkn = Ad*x0 + Bd*u0 + Ed*p0(:,i);
    yk = Cd*x0 + Dd*u0;
 

    
    X = [X, xkn];
    Y = [Y, yk];
    U = [U, u0];
    P = [P, p0(:,i)];
%     W = [W, w];
    
end

figure
% subplot(3,1,1)
% plot(time, X(:,1:end-1))
% title('States')
% legend('T floor','T internal facade','T external facade','T internal','location','bestoutside')
% xlabel('time')
% ylabel('temperature [\circC]')

subplot(3,1,1)
plot(time, Y,'LineWidth',2)
hold on
% plot(time,W,'r--','LineWidth',2)
title('room temperature')
legend('T room', 'reference','location','bestoutside')
xlabel('time')
ylabel('temperature [\circC]')

subplot(3,1,2)
stairs(time,U,'LineWidth',2)
title('Input')
legend('Q heating-cooling','location','bestoutside')
xlabel('time')
ylabel('Q')

subplot(3,1,3)
plot(time,P,'LineWidth',2)
title('Disturbances')
legend('T external','Q occupancy','Q solar','location','bestoutside')
xlabel('time')
ylabel('')