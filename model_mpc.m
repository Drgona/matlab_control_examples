%% Model Predictive Control

load('ss_model.mat')
load wpred_1979_may_Ts300.mat

base_Ts = y(2,1);

disturbances = [y(:,4)'; y(:,end-1)'; y(:,end)']; % T_ext, Q_occ, Q_sol

% MIMO model continuous
model1 = ss(A,B,C,D);

Ts = 15;
sd = c2d(ss(A, B, C, D), Ts);
Ad = sd.A;
Bd = sd.B(:, 2);
Cd = sd.C(4,:);
Ed = sd.B(:, [1 3 4]);
% Dd = sd.D;
Dd = 0;

% constraints
umin = [0];
umax = [5000];
ymin = [21.5];
ymax = [22.5];

% prediction horizon
N = 10;

%------------------------------------------------------
% MPC definition
nx = size(Ad, 2);
nu = size(Bd, 2);
ny = size(Cd, 1);

% penalty matices
Qu = [0.8e-4*eye(nu)];
Qs = 1e1;
Qy = 0; % output weight
tol = 0.5; % width of the zone

x = sdpvar(nx, N+1, 'full');
u = sdpvar(nu, N, 'full');
y = sdpvar(ny, N, 'full');
s = sdpvar(ny, N, 'full');
yref = sdpvar(ny, 1);      % for time varying reference: sdpvar(ny, N, 'full') format
con = [];
obj = 0;
for k = 1:N
    con = con + [ x(:, k+1) == Ad*x(:, k) + Bd*u(:, k) ];
    con = con + [ y(:, k) == Cd*x(:, k) + Dd*u(:, k) ];
    con = con + [ umin <= u(:, k) <= umax ];
    % con = con + [ xmin <= x(:, k) <= xmax ];
    % con = con + [ ymin-s(:, k) <= y(:, k) <= ymax+s(:, k) ];
%     if Qy==0
%         con = con + [ yref-tol-s(:, k) <= y(:, k) <= yref+tol+s(:, k) ];
%         con = con + [ s(:, k) >= 0 ];
%         obj = obj + s(:, k)'*Qs*s(:, k);
%     end
%     obj = obj + (y(:, k)-yref)'*Qy*(y(:, k)-yref) + u(:, k)'*Qu*u(:, k);
    con = con + [ yref-tol-s(:, k) <= y(:, k) <= yref+tol+s(:, k) ];
    obj = obj + s(:, k)'*Qs*s(:, k) + Qu*u(:, k);  % nie je treba kvadrat akcneho zasahu pretoze uvazujes len pozitivne hodnoty, t.j. kurenie
end
options = sdpsettings('verbose', 0);
% end of MPC definition
%------------------------------------------------------



%% Simulation
Nsim = 600; % number of simulation steps
% initial condition
x0 = [21; 21; 21; 21];
ref = 22;
p0 = disturbances(:,1:Nsim);

% % pre-compilation 
opt = optimizer(con, obj, [], {x(:, 1), yref}, u);

Xsim = x0;
Usim = [];
Ysim = [];
Psim = [];
Ref = [];
LB = [];
UB = [];

% out of interval values counter
counter = 0;

% closed-loop simulation
for k = 1:Nsim
    % skokova zmena
%     if k<Nsim/2
%         ref = 21;
%     else
%         ref = 22;
%     end
    if k == Nsim/4 | k == Nsim/2 | k == 0.75*Nsim | k == Nsim
        fprintf('Optimization complete on %d%%\n', (k*100)/Nsim);
    end
    
    xt = Xsim(:, end);
    [u, problem, info] = opt{{xt, ref}};
    if problem~=0
        error(info{1});
    end
    uopt = value(u(:, 1));
    Usim = [Usim, uopt];
    xn = Ad*xt + Bd*uopt + Ed*p0(:,k);% + Ed*poruchy(k, [1 3 4])';
    yn = Cd*xt + Dd*uopt;
    
    lb = ref - tol;
    ub = ref + tol;
    
    Xsim = [Xsim, xn];
    Ysim = [Ysim, yn];
    Psim = [Psim, p0(:,k)];
    
    Ref = [Ref, ref];
    LB = [LB, lb];
    UB = [UB, ub];
    
    if yn > ub | yn < lb
        counter = counter + 1;
    end
%     fprintf('counter = %d\n',counter)

end

% comfort
com_mpc = 100-(counter*100)/Nsim
% energy consumption
energy_mpc = sum(Usim)

close all
t = 0:Nsim-1;


figure
subplot(3,1,1)
plot(t,Ysim,'LineWidth',2)
hold on
plot(t,Ref,'r--','LineWidth',2)
plot(t,LB,'g--','LineWidth',2)
plot(t,UB,'g--','LineWidth',2)
title('Room Temperature')
legend('T room', 'reference','location','bestoutside')
xlabel('time')
ylabel('temperature [\circC]')

subplot(3,1,2)
stairs(t,Usim,'LineWidth',2)
title('Input')
legend('Q heating','location','bestoutside')
xlabel('time')
ylabel('Q')

subplot(3,1,3)
plot(t,Psim,'LineWidth',2)
title('Disturbances')
legend('T external','Q occupancy','Q solar','location','bestoutside')
xlabel('time')
ylabel('')
% end of closed-loop simulation
%------------------------------------------------------
